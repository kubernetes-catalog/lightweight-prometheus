# Updating manifests from latest release

There is a script in `/bin` which will update the manifests from the latest helm chart.

In the project root run
```bash
./bin update-manifests.sh
```

This will update the manifests.

Create a PR with the new manifests, 
mentioning the new version in the PR,
as well as linking to the new helm chart release,
so we can tag it appropriately afterwards.

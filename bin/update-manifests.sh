#!/usr/bin/env bash

CHART_REPO=https://prometheus-community.github.io/helm-charts
CHART_REPO_NAME=prometheus-community
CHART_NAME=prometheus

rm -rf manifests/* ./${CHART_NAME}/
helm repo add ${CHART_REPO_NAME} ${CHART_REPO}
helm repo update
helm fetch ${CHART_REPO_NAME}/${CHART_NAME} --untar
helm template ./${CHART_NAME} \
    --output-dir manifests \
    --namespace ${CHART_NAME} \
    --set kubeStateMetrics.enabled=false \
    ${CHART_NAME}
#rm -rf manifests/prometheus/templates/test
cp -r ./manifests/${CHART_NAME}/templates/* ./manifests/
pushd ./manifests/${CHART_NAME}/templates/
for d in */ ; do
pushd $d
cat <<EOF > kustomization.yml
resources:
$(for n in *; do printf '%s\n' "- $n"; done)
EOF
cp kustomization.yml ../../../${d}/
popd
done
for n in *; do printf '%s\n' "- $n"; done > ../../tmp-resources
cd ../../
cat <<EOF > kustomization.yml
resources:
$(cat tmp-resources)
EOF
popd
rm -rf ./manifests/${CHART_NAME} ./manifests/tmp-resources ./${CHART_NAME}
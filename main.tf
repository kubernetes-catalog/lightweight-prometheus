data "kustomization_overlay" "prometheus-prerequisites" {
  resources = [
    "${path.module}/pre-install-manifests",
  ]

  namespace = var.namespace
}

data "kustomization_overlay" "prometheus" {
  resources = [
    "${path.module}/manifests",
  ]

  namespace = var.namespace

  # We need to make sure that the ClusterRoleBinding namespace matches the namespace where we are deploying,
  # as Kustomize doesn't override this.
  # If the namespace is different from the default we provide, the deployment will fail
  patches {
    patch = <<-EOF
      - op: replace
        path: /subjects/0/namespace
        value: ${var.namespace}
    EOF
    target = {
      kind = "ClusterRoleBinding"
      name = "prometheus-alertmanager"
    }
  }

  patches {
    patch = <<-EOF
      - op: replace
        path: /subjects/0/namespace
        value: ${var.namespace}
    EOF
    target = {
      kind = "ClusterRoleBinding"
      name = "prometheus-pushgateway"
    }
  }

  patches {
    patch = <<-EOF
      - op: replace
        path: /subjects/0/namespace
        value: ${var.namespace}
    EOF
    target = {
      kind = "ClusterRoleBinding"
      name = "prometheus-server"
    }
  }

}

/**
 * When the Kustomization provider run through the manifests,
 * it cannot ensure that the namespace will be created first,
 * due to a ID limitation in the Terraform provider SDK.
 *
 * We therefor have a directory with prerequisites,
 * which we can install first,
 * and use that in a depends_on,
 * to ensure all prerequisites are met for our manifests.
 *
 * We also use a directory with a kustomization built in,
 * to ensure we can use the manifests without necessarily needing Terraform,
 * and could still use Kustomize directly on them.
 */
resource "kustomization_resource" "prometheus-prerequisites" {
  for_each = data.kustomization_overlay.prometheus-prerequisites.ids

  manifest = data.kustomization_overlay.prometheus-prerequisites.manifests[each.value]
}

/**
 * Once all of the prerequisites exist,
 * we can continue to create all of the other manifests,
 * and let Kubernetes take care of the rest for us.
 */
resource "kustomization_resource" "prometheus" {
  depends_on = [kustomization_resource.prometheus-prerequisites]

  for_each = data.kustomization_overlay.prometheus.ids

  manifest = data.kustomization_overlay.prometheus.manifests[each.value]
}

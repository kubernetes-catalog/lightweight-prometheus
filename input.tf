variable "kubeconfig" {
  type = string
  default = "~/.kube/config"
}

variable "namespace" {
  type = string
  default = "prometheus"
}
